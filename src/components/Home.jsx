import React from 'react';

function Home(){
  return(
    <div>
      <style jsx>{`
        img {
          width: 100%;
          border-radius: 60%;
          border: 2px solid #f1f1f1;
        }
      `}</style>
    <p>DoggoTime</p>
    </div>
  );
}

export default Home;
