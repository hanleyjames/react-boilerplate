import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';

function App(){
  return (
    <div>
      <Switch>
        <Route exact path='/' component={ Home } />
      </Switch>
    </div>
  );
}

export default App;
//<Route exact path='/' component={your main component here} />
